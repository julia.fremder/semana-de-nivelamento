// Executar exercícios na hora do código!

const readLine=require("readline-sync");

// 05 - Escreva um programa que percorra todos os valores de um array de objetos contendo como atributos o nome e o sexo dos alunos e permita que o usuário informe, 1 se o usuário esta presente ou 2 se o usuário faltou,  e após o usuário responder todos os itens o programa deverá exibir todos os itens da lista e o seu status de presença. Observação: para cada item da lista respondido o valor informado pelo usuário deverá ser armazenado para que possa estar sendo apresentado ao final do programa.

var pessoas = [{nome:'Julia', sexo:'F', presencaDiaX:''}, {nome:'Juliano', sexo:'M', presencaDiaX:''}, {nome:'Juliana', sexo:'F', presencaDiaX:''}, {nome:'Julio', sexo:'M', presencaDiaX:''}];

var pL = pessoas.length;
// console.log(pL);

for (var i3=0; i3 < pL; i3++){
  // console.log(pessoas[i3].nome)
  console.log('Preencha a lista de presencas dos alunos com 1 para presente e 2 para ausente:')
  r = readLine.question('O aluno ' + pessoas[i3].nome + ' esta presente?\n')
  if(r==1){
  pessoas[i3].presencaDiaX = 'presente';
  } else {
    pessoas[i3].presencaDiaX = 'ausente';
  }
  
}
console.log(pessoas);

